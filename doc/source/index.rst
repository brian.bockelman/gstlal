.. GstLAL documentation master file, created by
   sphinx-quickstart on Wed Apr 25 21:10:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

The GstLAL project documentation
================================

`GstLAL` provides a suite of GStreamer elements that expose gravitational-wave data analysis tools from the LALSuite library for use in GStreamer signal-processing pipelines.

Examples include an element to add simulated gravitational waves to an h(t) stream, and a source element to provide the contents of .gwf frame files to a GStreamer pipeline.

.. _welcome-contents:

Contents
-------------------------

.. toctree::
   :maxdepth: 2

   overview
   getting-started
   projects
   publications

.. _welcome-indices:

Indices and tables
-------------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Build/Test Results
-------------------------

Results pages for the `Offline Tutorial Test <https://git.ligo.org/lscsoft/gstlal/blob/master/gstlal-inspiral/tests/Makefile.offline_tutorial_test>`_ are generated automatically and are located here:

* `gstlal_offline_tutorial test dag <gstlal_offline_tutorial/1000000000-1000002048-test_dag-run_1/>`_
* `gstlal_offline_tutorial test dag lite <gstlal_offline_tutorial/1000000000-1000002048-test_dag-run_1_lite/>`_
