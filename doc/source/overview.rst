Overview
========

The GstLAL software package is used for the following activities:

- **GstLAL:** The package `GstLAL <http://software.ligo.org/lscsoft/source/gstlal-1.4.1.tar.gz>`_ provides core Gstreamer plugins for signal processing workflows with LIGO data and core python bindings for constructing such workflows.  

- **GstLAL Calibration:** The package `GstLAL Calibration <http://software.ligo.org/lscsoft/source/gstlal-calibration-1.2.4.tar.gz>`_ provides real-time calibration of LIGO control system data into strain data.

- **GstLAL Inspiral:** The package `GstLAL Inspiral <http://software.ligo.org/lscsoft/source/gstlal-inspiral-1.5.1.tar.gz>`_ provides additional signal processing plugins that are specific for LIGO / Virgo searches for compact binaries as well as a substantial amount of python code for post-processing raw signal processing results into gravitational wave candidate lists. Several publications about the methodology and workflow exist, see :ref:`publications`

- **GstLAL Ugly:** The package `GstLAL Inspiral <http://software.ligo.org/lscsoft/source/gstlal-inspiral-1.5.1.tar.gz>`_ is an incubator project for gradual inclusion in the other packages.


