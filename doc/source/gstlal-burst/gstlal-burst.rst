####################################################################################################
GstLAL burst
####################################################################################################

`GstLAL burst` contains several projects targeting a variety of different searches. These include:

  * **Feature extraction:** Identify noise transient bursts (glitches) in auxiliary channel data.
  * **Cosmic string search**
  * **Excess power**

Contents
-------------------------

.. toctree::
   :maxdepth: 2

   overview
   tutorials/tutorials
   code
