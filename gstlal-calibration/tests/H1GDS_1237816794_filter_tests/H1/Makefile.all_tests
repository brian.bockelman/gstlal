# This makefile can be used to run all of the tests instead of running them one by one.

# Indicate IFO and observing run
# Note: Make sure that there is no space after the L or H! 
# Another note: Livingston cluster cannot run this for IFO = H. Make sure that the cluster you're using has the raw frames you want. 
export IFO=H
# OBSRUN inidcates where to look for filters files (e.g. O1, O2, O3, ER10, ER13, ER14, PreER13, PreER14)
#export OBSRUN=O2
export OBSRUN=O3

# Set start and stop times for a short lock stretch
export SHORT_START=1237834163
export SHORT_END=1237834663
# Set start and stop times for a longer stretch that spans a lock-unlock stretch
export LONG_START = 1228350334
export LONG_END = 1228352985
export SHMRUNTIME=400

# How much time does the calibraiton need to settle at the start and end?
PLOT_WARMUP_TIME = 220
PLOT_COOLDOWN_TIME = 64
export SHORT_PLOT_START=$(shell echo $(SHORT_START) + $(PLOT_WARMUP_TIME) | bc)
export LONG_PLOT_START=$(shell echo $(LONG_START) + $(PLOT_WARMUP_TIME) | bc)
export SHORT_PLOT_END=$(shell echo $(SHORT_END) - $(PLOT_COOLDOWN_TIME) | bc) 
export LONG_PLOT_END=$(shell echo $(LONG_END) - $(PLOT_COOLDOWN_TIME) | bc) 

# Point to all of the .ini files for different configurations
#export GDSCONFIGS=H1GDS_1232821094_gstlal-calibration-1-2-7.ini
export GDSCONFIGS=test.ini 
#export GDSCONFIGSNOKAPPAS=H1GDS_1228712903_test_no_kappas.ini 

# Needed for writing pipeline graphs
export GST_DEBUG_DUMP_DOT_DIR=.

all: ASD_comparison statevector timeserieskappas response_function

# Create frame cache files for raw, C00, C01, and C02 frames
$(IFO)1_raw_frames.cache:
	gw_data_find -o $(IFO) -t $(IFO)1_R -s $(SHORT_START) -e $(SHORT_END) -l --url-type file > $@

$(IFO)1_C00_frames.cache:
	gw_data_find -o $(IFO) -t $(IFO)1_HOFT_C00 -s $(SHORT_START) -e $(SHORT_END) -l --url-type file > $@

$(IFO)1_C01_frames.cache:
	gw_data_find -o $(IFO) -t $(IFO)1_HOFT_C01 -s $(SHORT_START) -e $(SHORT_END) -l --url-type file > $@

$(IFO)1_C02_frames.cache:
	gw_data_find -o $(IFO) -t $(IFO)1_HOFT_C02 -s $(SHORT_START) -e $(SHORT_END) -l --url-type file > $@


# Calibrate GDS testing data
$(IFO)1_hoft_GDS_frames.cache: $(IFO)1_raw_frames.cache filters framesdir
	gstlal_compute_strain --gps-start-time $(SHORT_START) --gps-end-time $(SHORT_END) --frame-cache $(IFO)1_raw_frames.cache --output-path Frames/$(OBSRUN)/$(IFO)1/GDS/ --frame-duration=64 --frames-per-file=1 --wings=0 --config-file $(GDSCONFIGS)
	ls Frames/$(OBSRUN)/$(IFO)1/GDS/$(IFO)-$(IFO)1_TEST-*.gwf | lalapps_path2cache > $@

# Calibrate GDS testing data with no kappas applied for response function test
$(IFO)1_hoft_GDS_no_kappas_frames.cache: $(IFO)1_raw_frames.cache filters framesdir
	#gstlal_compute_strain --gps-start-time $(SHORT_START) --gps-end-time $(SHORT_END) --frame-cache $(IFO)1_raw_frames.cache --output-path Frames/$(OBSRUN)/$(IFO)1/GDS/ --frame-duration=64 --frames-per-file=1 --wings=0 --config-file $(GDSCONFIGSNOKAPPAS)
	#ls Frames/$(OBSRUN)/$(IFO)1/GDS/$(IFO)-$(IFO)1GDS_TEST_NOKAPPAS*.gwf | lalapps_path2cache > $@


filters:
	if [ -d Filters/$(OBSRUN)/GDSFilters ]; then \
                svn up Filters/$(OBSRUN)/GDSFilters; \
        else \
                mkdir -p Filters/$(OBSRUN); \
                cd Filters/$(OBSRUN); \
                svn co https://svn.ligo.caltech.edu/svn/aligocalibration/trunk/Runs/$(OBSRUN)/GDSFilters; \
        fi

framesdir:
	mkdir -p Frames/$(OBSRUN)/$(IFO)1/easy_raw
	mkdir -p Frames/$(OBSRUN)/$(IFO)1/GDS
	mkdir -p Frames/$(OBSRUN)/$(IFO)1/DCS


response_function: $(IFO)1_hoft_GDS_frames.cache $(IFO)1_raw_frames.cache
	make -f Makefile.response_function

ASD_comparison: $(IFO)1_raw_frames.cache $(IFO)1_hoft_GDS_frames.cache
	make -f Makefile.ASD_comparison

statevector: $(IFO)1_hoft_GDS_frames.cache
	make -f Makefile.statevector

timeserieskappas:  $(IFO)1_hoft_GDS_frames.cache 
	make -f Makefile.timeserieskappas

pcal_to_darm: $(IFO)1_raw_frames.cache $(IFO)1_hoft_GDS_frames.cache
	make -f Makefile.pcal_to_darm

# FIXME: This is seg faulting for me right now
calib_version_comparison: $(IFO)1_hoft_GDS_frames.cache $(IFO)1_C02_frames.cache
	make -f Makefile.calib_version_comparison

# Need to clean up this clean command
clean: 
	rm -rf Frames/
	rm -rf Filters/
	rm *.p*
	rm *.dot
	rm *.txt


