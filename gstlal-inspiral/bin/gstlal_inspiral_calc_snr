#!/usr/bin/env python

"""
Typical Usages:

--mode 0 (calculate SNR using LLOID method):
	1. GW options: also see datasource.GWDataSourceInfo()
	--frame-cache
	--data-source
	--channel-name
	--gps-start-time
	--gps-end-time
	--injection

	2. SVD bank options:
	--svd-bank (require)
	--bank-id (require)
	--row-number (optional, calculate all SNRs if not given)

	3. PSD options:
	--reference-psd (optional)
	--track-psd (default = False. If --reference-psd is not given, this will be set to True)
	--psd-fft-length (default = 32s)

	4. Output options:
	--output-width (default = 32bits)
	--instrument (require)
	--outdir (require)
	--start (default = None)
	--end (default = None)
	--verbose (default = False)
	--complex (defaule = False)

--mode 1 (calculate SNR using Finite Impulse Response):
	1. GW options: also see datasource.GWDataSourceInfo()
	--frame-cache
	--data-source
	--channel-name
	--gps-start-time
	--gps-end-time
	--injection

	2. Template options:
	--table (require)
	--approximant (require)
	--sample-rate (default = 2048Hz)
	--f-low (default = 10)
	--f-high (optional)
	--template-psd (require)

	3. PSD / Whiten options:
	--reference-psd (optional)
	--track-psd (default = False. If --reference-psd is not given, this will be set to True)
	--psd-fft-length (default = 32s)

	4. Output options:
	--output-width (default = 32bits)
	--instrument (require)
	--outdir (require)
	--start (default = None)
	--end (default = None)
	--verbose (default = False)
	--complex (default = False)
"""
import numpy
from optparse import OptionParser, OptionGroup, IndentedHelpFormatter
import os

from gstlal import datasource
from gstlal import pipeparts
from gstlal import reference_psd
from gstlal import svd_bank
from gstlal import svd_bank_snr

import lal
import lal.series

from ligo.lw import ligolw
from ligo.lw import utils as ligolw_utils
from ligo.lw import param as ligolw_param
from ligo.lw import array as ligolw_array
from ligo.lw import lsctables
from ligo.lw import table

@ligolw_param.use_in
@ligolw_array.use_in
@lsctables.use_in
class ContentHandler(ligolw.LIGOLWContentHandler):
	pass

# disable mkchecktimestamps()
# FIXME:  python plugin broken until we switch to Python 3
pipeparts.mkchecktimestamps = lambda pipeline, src, *args: src

class epilogFormatter(IndentedHelpFormatter):
	def format_epilog(self, epilog):
		if epilog:
			return epilog + "\n"
		else:
			return ""

def parse_command_line():
	parser = OptionParser(formatter = epilogFormatter(), description = "Using gstlal inspiral pipeline to calculate SNR for template(s)", epilog = __doc__)

	datasource.append_options(parser)

	group = OptionGroup(parser, "Whiten / PSD Options", "Adjust noise spectrum estimation parameter")
	group.add_option("--reference-psd", metavar = "filename", help = "Load noise spectrum from LIGO light-weight XML file (optional).")
	group.add_option("--psd-fft-length", metavar = "seconds", default = 32, type = "int", help = "Length of the FFT used to whiten strain data (default = 32 s).")
	group.add_option("--track-psd", action = "store_true", help = "Enable dynamic PSD tracking. Enabled by default if --reference-psd is not given.")
	group.add_option("--zero-pad", metavar = "seconds", default = 0, type = "int", help = "The zero padding of the Hanning window in seconds (default = 0).")
	group.add_option("--average-samples", default = 64, type = "int", help = "The number of samples used to estimate the average value of the PSD")
	group.add_option("--median-samples", default = 7, type = "int", help = "The number of samples used to estimate the median value of the PSD")
	parser.add_option_group(group)

	group = OptionGroup(parser, "Template Options", "Choose a template from a SVD bank file / a single SnglInspiral Table")
	group.add_option("--svd-bank", metavar = "filename", help = "A LIGO light-weight xml / xml.gz file containing svd bank information (require)." )
	group.add_option("--sub-bank-id", type = "int", help = "Bank id is of the form <int>ID_<int>N where N is the sub bank id. (require).")
	group.add_option("--row-number", type = "int", help = "The row number of the template (optional). All the SNRs will be outputed if it is not given.")
	group.add_option("--table", metavar = "filename", help = "A LIGO light-weight xml.gz file containing SnglInspiral Table. Expecting one template for each instrument only.")
	group.add_option("--approximant", metavar = "name", type = "str", help = "Name of the Waveform model (require).")
	group.add_option("--template-duration", metavar = "seconds", type = "float", help = "Duration of the template")
	group.add_option("--sample-rate", metavar = "Hz", default = 2048, type = "int", help = "Sampling rate of the template and SNR for mode 1")
	group.add_option("--f-low", metavar = "Hz", default = 10, type = "float", help = "The minimum frequency of GW signal")
	group.add_option("--f-high", metavar = "Hz", type = "float", help = "The maximum frequency of GW signal")
	group.add_option("--template-psd", metavar = "filename", help = "The psd for whitening template (require).")
	parser.add_option_group(group)

	# FIXME: implement it if needed
	#group = OptionGroup(parser, "Data Quality Options", "Adjust data quality handling")
	#group.add_option("--ht-gate-threshold", metavar= "sigma", type = "float", default = float("inf"), help = "Set the threshold on whitened h(t) to excise glitches in units of standard deviation (optional)")
	#group.add_option("--veto-segments-file", metavar = "filename", help = "Set the name of the LIGO light-weight XML file from which to load vetoes (optional).")
	#group.add_option("--veto-segments-name", metavar = "name", default = "vetoes", help = "Set the name of the LIGO light-weight XML file from which to load vetoes (default = 'veto') (optional).")
	#parser.add_option_group(group)

	group = OptionGroup(parser, "Output Control Options", "Control SNR output")
	group.add_option("--outdir", metavar = "directory", type = "str", help = "Output directory for SNR(s) (requires).")
	group.add_option("--mode", metavar = "method", type = "int", default = 0, help = "The method (0 = LLOID / 1 = FIR) that is used to calculate SNR (default = 0).")
	group.add_option("--complex", action = "store_true", help = "Choose whether to output the complex snr or not.")
	group.add_option("--start", metavar = "seconds", type = "int", help = "Start SNR time series at GPS time '--start'.")
	group.add_option("--end", metavar = "seconds", type = "int", help = "End SNR time series at GPS time '--end'.")
	group.add_option("--output-width", metavar = "bits", type = "int", default = 32, help = "The size of the output data, can only be 32 or 64 bits (default = 32 bits).")
	group.add_option("--instrument", metavar = "name", help = "The detector from which the --reference-psd and --frame-cache are loaded (require).")
	parser.add_option_group(group)

	parser.add_option("--verbose", action = "store_true", help = "Be verbsoe.")

	options, args = parser.parse_args()

	# Check SNR series output
	if options.start and options.end:
		if options.start >= options.end:
			raise ValueError("--start must less than --end.")

	# Setting up GW data
	gw_data_source_info = datasource.GWDataSourceInfo(options)
	if options.instrument not in gw_data_source_info.channel_dict.keys():
		raise ValueError("No such instrument: %s in GWDataSourceInfo: (%s)"% (options.instrument, ", ".join(gw_data_source_info.channel_dict.keys())))

	# Setting up PSD
	if options.reference_psd:
		psd = lal.series.read_psd_xmldoc(ligolw_utils.load_url(options.reference_psd, contenthandler = lal.series.PSDContentHandler))
		if options.instrument not in set(psd):
			raise ValueError("No such instrument: %s in psd: (%s)"% (options.instrument, ", ".join(set(psd))))
	else:
		options.track_psd = True

	# Use LLOID method
	if options.mode == 0:
		missing_required_options = []
		# Checking required options
		if options.svd_bank is None:
			missing_required_options.append("--svd-bank")
		if options.sub_bank_id is None:
			missing_required_options.append("--sub-bank-id")
		if options.outdir is None:
			missing_required_options.append("--outdir")
		if options.instrument is None:
			missing_required_options.append("--instrument")
		# Raise VauleError is required option(s) is/are missing
		if missing_required_options:
			raise ValueError("Missing required option(s) %s" % ", ".join(sorted(missing_required_options)))

		# Setting up SVD bank
		banks = svd_bank.read_banks(options.svd_bank, svd_bank.DefaultContentHandler)
		if banks is None:
			raise ValueError("svd bank is empty: Invaild --svd-bank %s" % options.svd_bank)
		else:
			if 0 <= options.sub_bank_id < len(banks) :
				bank = banks[options.sub_bank_id]
			else:
				raise ValueError("Invaild --sub-bank-id %d. Possible id [0-%d)\n" % (options.sub_bank_id, len(banks)))
		if not( 0 <= options.row_number < len(bank.sngl_inspiral_table) ):
			raise ValueError("No such template: Invaild --row-number %d. Possible range [0-%d)\n" % (options.row_number, len(bank.sngl_inspiral_table)))

		return options, gw_data_source_info, bank, psd

	# Use Finite Impulse Response
	elif options.mode == 1:
		missing_required_options = []
		# Checking required options
		if options.table is None:
			missing_required_options.append("--table")
		if options.approximant is None:
			missing_required_options.append("--approximant")
		if options.outdir is None:
			missing_required_options.append("--outdir")
		if options.instrument is None:
			missing_required_options.append("--instrument")
		if options.template_psd is None:
			missing_required_options.append("--template-psd")
		# Raise VauleError is required option(s) is/are missing
		if missing_required_options:
			raise ValueError("Missing required option(s) %s" % ", ".join(sorted(missing_required_options)))

		xmldoc = ligolw_utils.load_url(options.table, contenthandler = ContentHandler, verbose = options.verbose)
		template_table = table.get_table(xmldoc, lsctables.SnglInspiralTable.tableName)

		return options, gw_data_source_info, template_table, psd

	# Unknown mode
	else:
		raise ValueError("Invalid mode: %d" % options.mode)

options, gw_data_source_info, template, psd  = parse_command_line()
#====================================================================================================
#
#						main
#
#====================================================================================================

if options.mode == 0:
	bank = template
	num_of_row = bank.bank_fragments[0].mix_matrix.shape[1] / 2

	lloid_snr = svd_bank_snr.LLOID_SNR(
		gw_data_source_info,
		bank,
		options.instrument,
		psd = psd,
		psd_fft_length = options.psd_fft_length,
		track_psd = options.track_psd,
		width = options.output_width,
		verbose = options.verbose
	)

	if options.row_number is None:
		for index, snr in enumerate(lloid_snr(COMPLEX = options.complex, row_number = options.row_number, start = options.start, end = options.end)):
			snr.epoch += bank.sngl_inspiral_table[index].end
			snrdict = {options.instrument: [snr]}
			svd_bank_snr.write_url(svd_bank_snr.make_xmldoc(snrdict), os.path.join(options.outdir, "%s-SNR_%d-%d-%d.xml.gz" % (options.instrument, index, int(snr.epoch), int(snr.data.length * snr.deltaT))), verbose = options.verbose)
	else:
		lloidsnr = lloid_snr(COMPLEX = options.complex, row_number = options.row_number, start = options.start, end = options.end)
		lloidsnr[0].epoch += bank.sngl_inspiral_table[options.row_number].end
		snrdict = {options.instrument: lloidsnr}
		svd_bank_snr.write_url(svd_bank_snr.make_xmldoc(snrdict), os.path.join(options.outdir, "%s-SNR_%d-%d-%d.xml.gz" % (options.instrument, options.row_number, int(lloidsnr[0].epoch), int(lloidsnr[0].data.length * lloidsnr[0].deltaT))), verbose = options.verbose)
	#
	# uncomment to save all snrs in one single XML
	#
	#snrdict = {options.instrument : lloid_snr(COMPLEX = options.complex, row_number = options.row_number, drop_first = options.drop_first, drop_last = options.drop_last)}
	#svd_bank_snr.write_url(svd_bank_snr.make_xmldoc(snrdict), os.path.join(options.outdir, "%s-SNR-%d-%d.xml.gz" % (options.instrument, int(snrdict.[options.instrument][0].epoch), int(snrdict[options.instrument][0].data.length * snrdict[options.instrument][0].deltaT))), verbose = options.verbose)

elif options.mode == 1:
	template_table = template
	template, time_offset = svd_bank_snr.FIR_SNR.make_template(template_table, options.template_psd, options.sample_rate, options.approximant, options.instrument, options.f_low, f_high = options.f_high, verbose = options.verbose)

	#FIXME: proper handle for latency
	fir_snr = svd_bank_snr.FIR_SNR(
		gw_data_source_info,
		template,
		options.instrument,
		options.sample_rate,
		0,
		psd = psd,
		psd_fft_length = options.psd_fft_length,
		width = options.output_width,
		track_psd = options.track_psd,
		verbose = options.verbose
		)

	firsnr = fir_snr(COMPLEX = options.complex, start = options.start, end = options.end)
	firsnr[0].epoch += time_offset
	snrdict = {options.instrument : firsnr}
	svd_bank_snr.write_url(svd_bank_snr.make_xmldoc(snrdict),os.path.join(options.outdir, "%s-SNR-%d-%d.xml.gz" % (options.instrument, int(snrdict[options.instrument][0].epoch), int(snrdict[options.instrument][0].data.length * snrdict[options.instrument][0].deltaT))), verbose = options.verbose)
