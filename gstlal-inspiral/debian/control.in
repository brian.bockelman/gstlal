Source: gstlal-inspiral
Maintainer: Carsten Aulbert <carsten.aulbert@aei.mpg.de>
Section: lscsoft
Priority: optional
Standards-Version: 3.9.2
X-Python-Version: >= @MIN_PYTHON_VERSION@
Build-Depends:
 debhelper (>= 9),
 dh-python,
 doxygen (>= @MIN_DOXYGEN_VERSION@),
 fakeroot,
 gobject-introspection (>= @MIN_GOBJECT_INTROSPECTION_VERSION@),
 graphviz,
 gstlal-dev (>= @MIN_GSTLAL_VERSION@),
 gstlal-ugly-dev (>= @MIN_GSTLALUGLY_VERSION@),
 gtk-doc-tools (>= @MIN_GTK_DOC_VERSION@),
 lal-dev (>= @MIN_LAL_VERSION@),
 lal-python (>= @MIN_LAL_VERSION@),
 lalinspiral-dev (>= @MIN_LALINSPIRAL_VERSION@),
 lalinspiral-python (>= @MIN_LALINSPIRAL_VERSION@),
 lalmetaio-dev (>= @MIN_LALMETAIO_VERSION@),
 libgirepository1.0-dev (>= @MIN_GOBJECT_INTROSPECTION_VERSION@),
 libgsl-dev (>= 1.9),
 libgstreamer1.0-dev (>= @MIN_GSTREAMER_VERSION@),
 libgstreamer-plugins-base1.0-dev (>= @MIN_GSTREAMER_VERSION@),
 pkg-config (>= 0.18.0),
 python-all-dev (>= @MIN_PYTHON_VERSION@),
 python-glue (>= @MIN_GLUE_VERSION@),
 python-glue-ligolw-tools,
 python-gobject-dev

Package: gstlal-inspiral
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python:Depends},
 gstlal (>= @MIN_GSTLAL_VERSION@),
 gstlal-ugly (>= @MIN_GSTLALUGLY_VERSION@),
 lal (>= @MIN_LAL_VERSION@),
 lal-python (>= @MIN_LAL_VERSION@),
 lalinspiral (>= @MIN_LALINSPIRAL_VERSION@),
 lalinspiral-python (>= @MIN_LALINSPIRAL_VERSION@),
 lalmetaio (>= @MIN_LALMETAIO_VERSION@),
 libgirepository-1.0-1 (>= @MIN_GOBJECT_INTROSPECTION_VERSION@),
 libgstreamer1.0-0 (>= @MIN_GSTREAMER_VERSION@),
 libgstreamer-plugins-base1.0-0 (>= @MIN_GSTREAMER_VERSION@),
 python (>= @MIN_PYTHON_VERSION@),
 python-gi,
 python-glue (>= @MIN_GLUE_VERSION@),
 python-gobject,
 python-gst-1.0,
 python-h5py,
 python-ligo-gracedb (>= 1.11),
 python-ligo-lw (>= @MIN_LIGO_LW_VERSION@),
 python-ligo-segments (>= @MIN_LIGO_SEGMENTS_VERSION@),
 python-numpy (>= @MIN_NUMPY_VERSION@),
 python-scipy
Description: GStreamer for GW data analysis (inspiral parts)
 This package provides a variety of gstreamer elements for
 gravitational-wave data analysis and some libraries to help write such
 elements.  The code here sits on top of several other libraries, notably
 the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
 and, of course, GStreamer.
 This package contains plugins, libraries, and programs for inspiral data
 analysis.

Package: gstlal-inspiral-dev
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, 
 gstlal-dev (>= @MIN_GSTLAL_VERSION@),
 gstlal-inspiral (= ${binary:Version}),
 lal-dev (>= @MIN_LAL_VERSION@),
 lalinspiral-dev (>= @MIN_LALINSPIRAL_VERSION@),
 lalmetaio-dev (>= @MIN_LALMETAIO_VERSION@),
 libgsl-dev (>= 1.9),
 libgstreamer1.0-dev (>= @MIN_GSTREAMER_VERSION@),
 libgstreamer-plugins-base1.0-0 (>= @MIN_GSTREAMER_VERSION@),
 python-all-dev (>= @MIN_PYTHON_VERSION@),
 python-glue (>= @MIN_GLUE_VERSION@),
 python-gobject-dev
Description: Files and documentation needed for compiling gstlal-inspiral based plugins and programs.
 This package contains the files needed for building gstlal-inspiral based
 plugins and programs.
