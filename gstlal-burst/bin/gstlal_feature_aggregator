#!/usr/bin/env python

# Copyright (C) 2018  Patrick Godwin
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

__usage__ = "gstlal_feature_aggregator [--options]"
__description__ = "an executable to aggregate and generate job metrics for streaming features"
__author__ = "Patrick Godwin (patrick.godwin@ligo.org)"

#-------------------------------------------------
#                  Preamble
#-------------------------------------------------

from collections import defaultdict, deque
import json
from multiprocessing.dummy import Pool as ThreadPool
import optparse
import os
import signal
import sys
import time

import numpy

from confluent_kafka import Consumer, KafkaError

from ligo.scald import aggregator
from ligo.scald import io

from gstlal.fxtools import utils

#-------------------------------------------------
#                  Functions
#-------------------------------------------------

def parse_command_line():

    parser = optparse.OptionParser(usage=__usage__, description=__description__)
    group = optparse.OptionGroup(parser, "Aggregator Options", "General settings for configuring the online aggregator.")
    group.add_option("-v","--verbose", default=False, action="store_true", help = "Print to stdout in addition to writing to automatically generated log.")
    group.add_option("--log-level", type = "int", default = 10, help = "Sets the verbosity of logging. Default = 10.")
    group.add_option("--rootdir", metavar = "path", default = ".", help = "Location where log messages and sqlite database lives")
    group.add_option("--tag", metavar = "string", default = "test", help = "Sets the name of the tag used. Default = 'test'")
    group.add_option("--sample-rate", type = "int", metavar = "Hz", default = 1, help = "Set the sample rate for feature timeseries output, must be a power of 2. Default = 1 Hz.")
    group.add_option("--processing-cadence", type = "float", default = 0.1, help = "Rate at which the aggregator acquires and processes data. Default = 0.1 seconds.")
    group.add_option("--request-timeout", type = "float", default = 0.2, help = "Timeout for requesting messages from a topic. Default = 0.2 seconds.")
    group.add_option("--kafka-server", metavar = "string", help = "Sets the server url that the kafka topic is hosted on. Required.")
    group.add_option("--input-topic-basename", metavar = "string", help = "Sets the input kafka topic basename. Required.")
    group.add_option("--jobs", action="append", help="Specify jobs to process. Can be given multiple times.")
    group.add_option("--data-backend", default="hdf5", help = "Choose the backend for data to be stored into, options: [hdf5|influx]. default = hdf5.")
    group.add_option("--influx-hostname", help = "Specify the hostname for the influxDB database. Required if --data-backend = influx.")
    group.add_option("--influx-port", help = "Specify the port for the influxDB database. Required if --data-backend = influx.")
    group.add_option("--influx-database-name", help = "Specify the database name for the influxDB database. Required if --data-backend = influx.")
    group.add_option("--data-type", metavar = "string", help="Specify datatypes to aggregate from 'min', 'max', 'median'. Default: max")
    group.add_option("--num-processes", type = "int", default = 2, help = "Number of processes to use concurrently, default 2.")
    parser.add_option_group(group)

    options, args = parser.parse_args()

    return options, args

#-------------------------------------------------
#                   Classes
#-------------------------------------------------

class StreamAggregator(object):
    """
    Ingests and aggregates incoming streaming features, collects job metrics.
    """
    def __init__(self, logger, options):
        logger.info('setting up feature aggregator...')

        ### initialize timing options
        self.request_timeout = options.request_timeout
        self.processing_cadence = options.processing_cadence
        self.sample_rate = options.sample_rate
        self.is_running = False

        ### kafka settings
        self.kafka_settings = {
            'bootstrap.servers': options.kafka_server,
            'group.id': 'aggregator_%s_%s'%(options.tag, options.jobs[0])
        }

        ### other aggregator options
        self.data_type = options.data_type
        self.last_save = aggregator.now()

        ### initialize consumers
        self.jobs = options.jobs
        self.consumer_names = ['%s_%s' % (options.input_topic_basename, job) for job in self.jobs]
        self.job_consumers = [(job, Consumer(self.kafka_settings)) for job, topic in zip(self.jobs, self.consumer_names)]
        for topic, job_consumer in zip(self.consumer_names, self.job_consumers):
            job_consumer[1].subscribe([topic])

        ### initialize 30 second queue for incoming buffers
        self.feature_queue = {job: deque(maxlen = 30 * self.sample_rate) for job in self.jobs}

        ### set up aggregator 
        logger.info("setting up aggregator with backend: %s"%options.data_backend)
        if options.data_backend == 'influx':
            self.agg_sink = io.influx.Aggregator(
                hostname=options.influx_hostname,
                port=options.influx_port,
                db=options.influx_database_name,
                reduce_across_tags=False,
            )
        else: ### hdf5 data backend
            self.agg_sink = io.hdf5.Aggregator(
                rootdir=options.rootdir,
                num_processes=options.num_processes,
                reduce_across_tags=False,
            )

        ### define measurements to be stored from aggregators
        self.agg_sink.register_schema('latency', columns='data', column_key='data', tags='job', tag_key='job')
        self.agg_sink.register_schema('snr', columns='data', column_key='data', tags=('channel', 'subsystem'), tag_key='channel')

    def fetch_data(self, job_consumer):
        """
        requests for a new message from an individual topic,
        and add to the feature queue
        """
        job, consumer = job_consumer
        message = consumer.poll(timeout=self.request_timeout)

        ### only add to queue if no errors in receiving data
        if message and not message.error():

            ### decode json and parse data
            feature_subset = json.loads(message.value())
            self.feature_queue[job].appendleft((feature_subset['timestamp'], feature_subset['features']))

    def fetch_all_data(self):
        """
        requests for a new message from all topics, and add
        to the feature queue
        """
        pool = ThreadPool(len(self.job_consumers))
        result = pool.map_async(self.fetch_data, self.job_consumers)
        result.wait()
        pool.close()

    def process_queue(self):
        """
        process and aggregate features from feature extraction jobs on a regular cadence
        """
        if utils.in_new_epoch(aggregator.now(), self.last_save, 1):
            self.last_save = aggregator.now()

            ### format incoming packets into metrics and timeseries
            feature_packets = [(job, self.feature_queue[job].pop()) for job in self.jobs for i in range(len(self.feature_queue[job]))]
            all_timeseries, all_metrics = self.packets_to_timeseries(feature_packets)

            ### store and aggregate metrics
            metric_data = {job: {'time': metrics['time'], 'fields': {'data': metrics['latency']}} for job, metrics in all_metrics.items()}
            self.agg_sink.store_columns('latency', metric_data, aggregate=self.data_type)

            ### store and aggregate features
            timeseries_data = {(channel, self._channel_to_subsystem(channel)): {'time': timeseries['trigger_time'], 'fields': {'data': timeseries['snr']}} for channel, timeseries in all_timeseries.items()}
            self.agg_sink.store_columns('snr', timeseries_data, aggregate=self.data_type)

            try:
                max_latency = max(max(metrics['latency']) for metrics in all_metrics.values())
                logger.info('processed features at time %d, highest latency is %.3f' % (self.last_save, max_latency))
            except:
                logger.info('no features to process at time %d' % self.last_save)

    def packets_to_timeseries(self, packets):
        """
        splits up a series of packets into ordered timeseries, keyed by channel
        """
        metrics = defaultdict(lambda: {'time': [], 'latency': []})

        ### process each packet sequentially and split rows by channel
        channel_rows = defaultdict(list)
        for job, packet in packets:
            timestamp, features = packet
            metrics[job]['time'].append(timestamp)
            metrics[job]['latency'].append(utils.gps2latency(timestamp))
            for channel, row in features.items():
                channel_rows[channel].extend(row) 

        ### break up rows into timeseries
        timeseries = {}
        for channel, rows in channel_rows.items():
             timeseries[channel] = {column: [row[column] for row in rows] for column in rows[0].keys()}

        return timeseries, metrics

    def start(self):
        """
        starts ingestion and aggregation of features
        """
        logger.info('starting feature aggregator...')
        self.is_running = True
        while self.is_running:
            ### ingest incoming features
            self.fetch_all_data()
            ### push combined features downstream
            self.process_queue()
            ### repeat with processing cadence
            time.sleep(self.processing_cadence)

    def stop(self):
        """
        shut down gracefully
        """
        logger.info('shutting down feature aggregator...')
        self.is_running = False

    @staticmethod
    def _channel_to_subsystem(channel):
        """
        given a channel, returns the subsystem the channel lives in
        """
        return channel.split(':')[1].split('-')[0]

class SignalHandler(object):
    """
    helper class to shut down the stream aggregator gracefully before exiting
    """
    def __init__(self, aggregator_sink, signals = [signal.SIGINT, signal.SIGTERM]):
        self.aggregator_sink = aggregator_sink
        for sig in signals:
            signal.signal(sig, self)

    def __call__(self, signum, frame):
        self.aggregator_sink.stop()
        sys.exit(0)


#-------------------------------------------------
#                    Main
#-------------------------------------------------

if __name__ == '__main__':
    # parse arguments
    options, args = parse_command_line()

    ### set up logging
    logger = utils.get_logger(
        '-'.join([options.tag, 'feature_aggregator', options.jobs[0]]),
        log_level=options.log_level,
        rootdir=options.rootdir,
        verbose=options.verbose
    )

    # create summary instance
    aggregator_sink = StreamAggregator(logger, options=options)

    # install signal handler
    SignalHandler(aggregator_sink)

    # start up listener
    aggregator_sink.start()
